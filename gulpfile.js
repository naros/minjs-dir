'use strict';

var gulp            = require('gulp');
var path            = require('path');
var uglify          = require('gulp-uglify');
var rename          = require('gulp-rename');
var clean           = require('gulp-clean');

var sourceFolders   = './sourceFolders';
var targetFolders   = './targetFolders';

gulp.task('minjs', function () {
  var allFile       = path.join(sourceFolders, '**/**.*');
  var allJsFile     = path.join(sourceFolders, '**/**.js');
  var allJsMinFile  = path.join(sourceFolders, '**/*.min.js');
  var allJsTestFile = path.join(sourceFolders, '**/*_test.js');

  // create min files
  gulp.src([allJsFile, '!' + allJsTestFile, '!' + allJsMinFile])
       .pipe(uglify({ mangle: false }))
       .pipe(rename({ suffix: '.min' }))
       .pipe(gulp.dest(targetFolders));

  // copy other files (exclude .min.js)
  gulp.src([allFile, '!' + allJsMinFile])
       .pipe(gulp.dest(targetFolders));

});

gulp.task('clean', function () {
  var allSourceFile = path.join(sourceFolders, '**/**.*');
  var allTargetFile = path.join(targetFolders, '**/**.*');
  var allSourceDir  = path.join(sourceFolders, '**/**');
  var allTargetDir  = path.join(targetFolders, '**/**');
  gulp.src(
    [ allSourceFile, allTargetFile,
      '!' + sourceFolders, '!' + targetFolders,
      allSourceDir, allTargetDir
    ],
    {read: false}
  ).pipe(clean());
});
